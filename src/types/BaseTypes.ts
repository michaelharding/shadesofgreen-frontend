import { ActionTypes } from "./ActionTypes";

export interface IBaseAction {
   type: ActionTypes,
   payload: object | string | number,
}

export interface IBaseState {
   loading: boolean,
   error?: string,
}