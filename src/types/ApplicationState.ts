import { IPagesState } from "./PageTypes";
import { IPostsState } from "./PostTypes";

export default interface IApplicationState {
   posts: IPostsState,
   pages: IPagesState,
}