import { IBaseAction, IBaseState } from "./BaseTypes";

export interface IPost {
  id: string;
  title: string;
  slug: string;
  date: string;
  tags: string[];
  category: string[];
  content: {
    html: string;
  };
  photos: [
    {
      id: string;
      lens: string;
      dateTaken: string;
      imageUrl: string;
      image: {
        id: string;
        fileName: string;
        url: string;
      };
    }
  ];
}

export interface IPostsState extends IBaseState {
  posts: IPost[];
  postCount: number;
  pageSize: number;
  post?: IPost;
}

export interface IPostAction extends IBaseAction {
  payload: IPost[] | IPost | string | number;
}
