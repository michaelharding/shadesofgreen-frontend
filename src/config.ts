const SERVER_URL =
  process.env.REACT_APP_SERVER_URL ||
  "https://api-us-east-1.graphcms.com/v2/ckuvpo04l22tz01z1h5869715/master";

const config = {
  ROOT_URL: SERVER_URL,
};

export default config;
